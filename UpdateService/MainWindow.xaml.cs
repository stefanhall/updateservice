﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UpdateService.Controllers;
namespace UpdateService
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainController controller = new MainController();
        public MainWindow()
        {
            
            InitializeComponent();
            cmbServices.ItemsSource = controller.GetJeevesServices();
        }

        private void cmbServices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtServiceName.Text = cmbServices.SelectedValue.ToString();
            txtServiceDesc.Text = controller.GetDescription(cmbServices.SelectedValue.ToString());
        }

        private void btnUpdateDesc_Click(object sender, RoutedEventArgs e)
        {
            string _oldDescription = controller.GetDescription(cmbServices.SelectedValue.ToString());

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C sc description " + cmbServices.SelectedValue.ToString() + '\u0020' + '\u0022' + txtServiceDesc.Text + '\u0022';
            process.StartInfo = startInfo;
            process.Start();
            MessageBox.Show("Ändrat beskrivning på tjänst " + cmbServices.SelectedValue.ToString() + " från " + _oldDescription + " till " + txtServiceDesc.Text);
        }

        private void btnUpdateName_Click(object sender, RoutedEventArgs e)
        {
            string _oldName = cmbServices.SelectedValue.ToString();

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C sc config " + _oldName + " displayname= " + '\u0022' + txtServiceName.Text + '\u0022';
            process.StartInfo = startInfo;
            process.Start();
            MessageBox.Show(startInfo.Arguments);
            MessageBox.Show("Ändrat namnet på tjänsten från " + _oldName + " till " + txtServiceName.Text);

        }
    }
}
