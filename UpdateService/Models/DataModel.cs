﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;

namespace UpdateService.Models
{
    public class DataModel
    {
        public List<string> GetServices()
        {
            List<string> _lstJeevesServices = new List<string>();
            ServiceController[] _jeevesServices;
            _jeevesServices = ServiceController.GetServices();
            
            foreach (ServiceController service in _jeevesServices)
            {
                if(service.DisplayName.Length >= 8)
                    if (service.DisplayName.Substring(0, 8) == "JeevesCB")
                        _lstJeevesServices.Add(service.DisplayName);
            }
            
            return _lstJeevesServices;
        }

        public string GetServiceDescription(string serviceName)
        {
            using (System.Management.ManagementObject service = new System.Management.ManagementObject(new System.Management.ManagementPath(string.Format("Win32_Service.Name='{0}'", serviceName))))
            {
                return service["Description"].ToString();
            }
                
        }
    }
}
