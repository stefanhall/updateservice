﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UpdateService.Models;
namespace UpdateService.Controllers
{
    public class MainController
    {
        DataModel dataModel = new DataModel();
        public List<string> GetJeevesServices()
        {
            
            List<string> lstJeevesServices = dataModel.GetServices();
            return lstJeevesServices;
        }
        public string GetDescription(string _serviceName)
        {
            return dataModel.GetServiceDescription(_serviceName);
        }
    }
}
